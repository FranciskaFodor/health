package hu.franciskafodor.health;

public class BmiCalculator {
	private double bmi;
	private double weight;
	private double height;
	
	public double getWeight() {
		return weight;
	}

	public double getHeight() {
		return height;
	}
	
	public BmiCalculator(double weight, UnitWeight weightUnit, double height, UnitHeight heightUnit) throws BmiCalculatorException {
		if (weight < 0) throw new BmiCalculatorException("Weight has to be positive! weight: " + weight);
		if (height < 0) throw new BmiCalculatorException("Height has to be positive! height: " + height);
		this.weight = convertWeightToKg(weight, weightUnit);
		this.height = convertHeightToMeter(height, heightUnit);
	}

	private double convertWeightToKg(double weight, UnitWeight weightUnit) {
		double weightInKg = 0;
		
		switch (weightUnit) {
		case GRAM:
			weightInKg = weight / (double)1000;
			break;
		case DEKAGRAM:
			weightInKg = weight / (double)100;
			break;
		case KILOGRAM:
			weightInKg = weight;
			break;			
		default:
			throw new IllegalArgumentException("Unknown weight measurement unit! weightMUnit: " + weightUnit);
		};
		
		return weightInKg;
	}
	
	private double convertHeightToMeter(double height, UnitHeight heightUnit) {
		double heightInMeter = 0;
		
		switch (heightUnit) {
		case CENTIMETER:
			heightInMeter = height / (double)100;
			break;
		case INCH:
			heightInMeter = Math.round(height * (double)0.0254 * 10) / (double)10;
			break;	
		case METER:
			heightInMeter = height;
			break;
		default:
			throw new IllegalArgumentException("Unknown height measurement unit! heightUnit: " + heightUnit);
		};
		
		return heightInMeter;
	}

	public double calculateBmi() {
		bmi = Math.round(weight / Math.pow(height, 2) * 10) / (double)10;
		return bmi; 
	}
	
	public String weightStatus() {		
		if(bmi < 16) return "Severe Thinness";
		else if(bmi < 17) return "Moderate Thinness";
		else if(bmi < 18.5) return "Mild Thinness";
		else if(bmi < 25) return "Normal";
		else if(bmi < 30) return "Overweight";
		else if(bmi < 35) return "Obese Class I";
		else if(bmi < 40) return "Obese Class II";
		else return "Obese Class III";
	}
}
