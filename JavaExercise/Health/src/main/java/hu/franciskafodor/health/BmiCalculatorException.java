package hu.franciskafodor.health;

public class BmiCalculatorException extends Exception {

	public BmiCalculatorException(String message) {
		super(message);
	}
}
