package hu.franciskafodor.health;

public enum UnitWeight {
	KILOGRAM,
	DEKAGRAM,
	GRAM;
}
