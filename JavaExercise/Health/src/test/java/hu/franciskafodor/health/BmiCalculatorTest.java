package hu.franciskafodor.health;

import static org.junit.Assert.*;
import org.junit.Test;

public class BmiCalculatorTest {
		
	private static final double WEIGHT_70KG = 70;
	private static final double WEIGHT_7000DKG = 7000;
	private static final double WEIGHT_70000G = 70000;
	private static final double WEIGHT_NEGATIVE = -1;	
	private static final double HEIGHT_1_8M = 1.8;
	private static final double HEIGHT_NEGATIVE = -2;
	private static final double HEIGHT_180CM = 180;
	private static final double HEIGHT_INCH_70_9 = 70.9;	
	private static final double BMI_21_6 = 21.6;
	private static final double DOUBLE_PRECISION = 0;
	
	@Test
	public void shouldCalculateBmiInKgInMeter() throws BmiCalculatorException {
		double result = new BmiCalculator(WEIGHT_70KG, UnitWeight.KILOGRAM, HEIGHT_1_8M, UnitHeight.METER).calculateBmi();	
		assertEquals(BMI_21_6, result, DOUBLE_PRECISION);
	}
	
	@Test
	public void shouldCalculateBmiInDkgInMeter() throws BmiCalculatorException {
		double result = new BmiCalculator(WEIGHT_7000DKG, UnitWeight.DEKAGRAM, HEIGHT_1_8M, UnitHeight.METER).calculateBmi();		
		assertEquals(BMI_21_6, result, DOUBLE_PRECISION);
	}	
	
	@Test(expected = BmiCalculatorException.class)
	public void shouldNotAcceptNegativeWeight() throws BmiCalculatorException {
		new BmiCalculator(WEIGHT_NEGATIVE, UnitWeight.KILOGRAM, HEIGHT_1_8M, UnitHeight.METER).calculateBmi();
	}	
	
	@Test(expected = BmiCalculatorException.class)
	public void shouldNotAcceptNegativeHeight() throws BmiCalculatorException {
		new BmiCalculator(WEIGHT_70KG, UnitWeight.KILOGRAM, HEIGHT_NEGATIVE, UnitHeight.METER).calculateBmi();
	}
	
	@Test
	public void shouldCalculateBmiInKgInCentimeter() throws BmiCalculatorException {
		double result = new BmiCalculator(WEIGHT_70KG, UnitWeight.KILOGRAM, HEIGHT_180CM, UnitHeight.CENTIMETER).calculateBmi();		
		assertEquals(BMI_21_6, result, DOUBLE_PRECISION);
	}
	
	@Test
	public void shouldCalculateBmiInKgInInch() throws BmiCalculatorException {
		double result = new BmiCalculator(WEIGHT_70KG, UnitWeight.KILOGRAM, HEIGHT_INCH_70_9, UnitHeight.INCH).calculateBmi();		
		assertEquals(BMI_21_6, result, DOUBLE_PRECISION);
	}
	
	@Test
	public void shouldCalculateBmiInGramInInch() throws BmiCalculatorException {
		double result = new BmiCalculator(WEIGHT_70000G, UnitWeight.GRAM, HEIGHT_INCH_70_9, UnitHeight.INCH).calculateBmi();		
		assertEquals(BMI_21_6, result, DOUBLE_PRECISION);
	}
	
	@Test(expected = BmiCalculatorException.class)
	public void shouldNotAcceptNegativeWeightAndNegativeHeight() throws BmiCalculatorException {
		new BmiCalculator(WEIGHT_NEGATIVE, UnitWeight.KILOGRAM, HEIGHT_NEGATIVE, UnitHeight.INCH).calculateBmi();
	}
}
